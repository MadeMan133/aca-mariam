package tests.seleniumTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;


public class RahulAcademy {



    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\MSI-PC\\Desktop\\drivers2\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(50));
        driver.manage().window().maximize();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");


        WebElement checkRatio = driver.findElement(By.xpath("//input[@value = 'radio1']"));
        checkRatio.click();


        WebElement searchCountry = driver.findElement(By.id("autocomplete"));
        searchCountry.sendKeys("Armenia");

        WebElement dropDown = driver.findElement(By.id("dropdown-class-example"));
        selectFromDropDown(dropDown,"Option1");

        WebElement chckBoxSelect = driver.findElement(By.id("checkBoxOption1"));
        chckBoxSelect.click();

        String winHandleBefore = driver.getWindowHandle();
        WebElement openWidow = driver.findElement(By.id("openwindow"));
        openWidow.click();

        //Switch to new window opened
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        // Perform the actions on new window
        driver.close(); //this will close new opened window

        //switch back to main window using this code
        driver.switchTo().window(winHandleBefore);

//        WebElement openTap = driver.findElement(By.id("opentab"));
//        openTap.click();

        WebElement enterName = driver.findElement(By.id("name"));
        enterName.sendKeys("Mariam");

        WebElement alertBtn = driver.findElement(By.id("alertbtn"));
        alertBtn.click();

        WebElement confirmBtn = driver.findElement(By.id("confirmbtn"));
        confirmBtn.click();




//        WebElement model = driver.findElement(By.xpath("//*[@id='v-model']"));
//        selectFromDropDown(model,"Model Y");
//
//        WebElement year = driver.findElement(By.name("year[gt]"));
//        selectFromDropDown(year, "2018");
//
//        WebElement checkBox = driver.findElement(By.xpath("//span[@class=\"lever\"]\n"));
//        checkBox.click();
//
//        WebElement submitForm = driver.findElement(By.id("search-btn"));
//        submitForm.click();






//        Select yearSelect = new Select(driver.findElement(By.name("year[gt]")));
//        yearSelect.selectByValue("2018");

    }




    public static void selectFromDropDown(WebElement webElem, String optionValue){
        Select select = new Select(webElem);
        select.selectByVisibleText(optionValue);
    }
    
}
